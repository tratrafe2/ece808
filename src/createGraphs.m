%{
% This program is responsible for reading .csv files as outputed from the
% java RSA implementation for the ECE807 project. It then plots the 
% appropriate figures and finally saves them to a folder. In order to run,
% the files and folders need to be set up correctly.
%}
%%
clear all;

%% Read files
T(:,:,1) = readmatrix('../metrics/metrics_1.csv');
T(:,:,2) = readmatrix('../metrics/metrics_2.csv');
T(:,:,3) = readmatrix('../metrics/metrics_3.csv');
T(:,:,4) = readmatrix('../metrics/metrics_4.csv');
T(:,:,5) = readmatrix('../metrics/metrics_5.csv');
T(:,:,6) = readmatrix('../metrics/metrics_6.csv');

%% Strings

titles=[
    "Path size heuristic.";
    "Slots Number heuristic.";
    "Confidential demands last.";
    "FIFO heuristic.";
    "Hybrid Path size."
    "Hybrid Slots Number."
];

names=[
  "Initial Connetions";
  "Computation time (ms)";
  
  "Confidential demands";
  "Routed Confidential demands";
  "Utilized slots of Confidential demands";
 
  "Normal demands";
  "Routed Normal demands";
  "Utilized slots of Normal demands";
  
  "Average XOR per confidential demand";
  
  "Not routed demands";
  "Blocking rate";
];



%% Calculate not routed and blocking rate metrics
for i=1:size(T,3)
    T(:,10,i)=T(:,1,i)-T(:,4,i)-T(:,7,i);
    T(:,11,i)=100*T(:,10,i)./T(:,1,i);
end

%% Plot graphs
X=[2;3;4;5;6;7;8;9;10;11];
%% Heuristics
plotColumns(X,names,T,titles);


%% Plot Utilized Slots
for counter=1:size(T,3)
    t=T(:,:,counter);
    name={'Utilized Slots',titles(counter)};
    figure('Name',strcat(name{1}," ",name{2}));
    title(name);
    xlabel("Number of connections");
    ylabel('Utilized Slots');
    x=t(:,1);
    y1=t(:,5)+t(:,8);
    y2=t(:,5);
    hold on;
    axis([0 3000 0 8000])
    bar(x,y1);
    bar(x,y2);
    hold off;
    lg=legend('Confidential + Normal','Confidential','Location','best');
end

%% Save plots 
folder = "../figures"; 
figList = findobj(allchild(0), 'flat', 'Type', 'figure');
for i = 1:length(figList)
  figHandle = figList(i);
  figName   = get(figHandle, 'Name');
  saveas(figHandle, fullfile(folder, strcat(figName,int2str(i),'.png')));
end

%% Functions
function [] = plotColumns(X,names,T,titles)
    for counterX=1:size(X,1)
        i=X(counterX);
        name=names(i);
        figure('Name',strcat(name,""));
        title(name);
        xlabel("Number of connections");
        ylabel(names(i));
        hold on;
        for counterT=1:size(T,3)
            plot(T(:,1,counterT),T(:,i,counterT));
        end
        hold off;
        lg=legend(titles,'Location','best');
    end
end
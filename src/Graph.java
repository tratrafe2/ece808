import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Graph class is the one responsible for the creation from file, traversal
 * and presentation of the Graph objects that will be used in the project. Every
 * algorithm regarding graph theory used in this project, as mentioned in
 * Project Outline, is implemented in this file. For the computation of the
 * primary and backup paths of the connections, graph traversal algorithms that
 * find the shortest path between a source and a destination node were used: 1.
 * Dijkstra: Guarantees that the primary path is the optimal path, but does not
 * guarantee the existence of a link disjointed or node and link disjointed
 * backup path. It runs only on zero or positive cost edges. It was implemented
 * using the min-heap approach which lowers the complexity of the original
 * algorithm. 2. Bellman-Ford: Similar to the Dijkstra algorithm, but can also
 * run on graphs with negative cost edges. It is also able to report
 * negative-weight cycles. 3. Suurballe: is guaranteed to find two link
 * disjointed paths if they exist, without guaranteeing that the primary path
 * will be optimal. This algorithm can be extended to also find two node and
 * link disjoint paths, but for this project’s implementation only the link
 * disjoint version can be used. For every one of the above algorithms, the
 * Graph.java file is also responsible to decode their solution into a compact
 * output which is either a path (for options 1 and 2) or a graph (for option
 * 3). Furthermore, it holds functions that can transform a graph, given a path
 * into a link-disjointed or a node-disjointed graph.
 * 
 * @author cmakri07
 *
 */
public class Graph {
	private int[][] edges;

	/**
	 * The graph constructor.
	 * 
	 * @param n Number of nodes.
	 */
	public Graph(int n) {
		edges = new int[n][n];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				edges[i][j] = 0;
	}

	/**
	 * A helping function in order to completely copy a graph.
	 * 
	 * @return The copy of the graph.
	 */
	private Graph copyGraph() {
		int n = this.edges.length;
		Graph g = new Graph(n);
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				g.edges[i][j] = this.edges[i][j];
		return g;
	}

	/**
	 * It copies a graph and given a path it assures that the links of that path are
	 * disjoint from the graph.
	 * 
	 * @param path The path to be disjoint
	 * @return The altered graph.
	 */
	public Graph copyLinkDisjointed(ArrayList<Integer> path) {
		Graph g = copyGraph();
		if (path == null)
			return g;
		for (int i = 0; i < path.size() - 1; i++) {
			int u = path.get(i);
			int v = path.get(i + 1);
			g.edges[u][v] = 0;
			g.edges[v][u] = 0;
		}
		return g;
	}

	/**
	 * It copies a graph and given a path it assures that the links and nodes of
	 * that path are disjoint from the graph.
	 * 
	 * @param path The path to be disjoint
	 * @return The altered graph.
	 */
	public Graph copyNodeLinkDisjointed(ArrayList<Integer> path) {
		Graph g = copyGraph();
		if (path == null)
			return g;
		for (int i = 1; i < path.size() - 1; i++) {
			int u = path.get(i);
			for (int x = 0; x < g.edges.length; x++) {
				g.edges[u][x] = 0;
				g.edges[x][u] = 0;
			}
		}
		int start = path.get(0);
		int start_next = path.get(1);
		int end_prev = path.get(path.size() - 2);
		int end = path.get(path.size() - 1);
		g.edges[start][start_next] = 0;
		g.edges[start_next][start] = 0;
		g.edges[end][end_prev] = 0;
		g.edges[end_prev][end] = 0;
		return g;
	}

	/**
	 * It reads a graph from a file.
	 * 
	 * @param filePath The file path of the graph
	 * @return The graph created
	 */
	public static Graph graphFromFile(String filePath) {
		try {
			Scanner sc = new Scanner(new File(filePath));
			int n = sc.nextInt();
			Graph g = new Graph(n);
			while (sc.hasNextLine()) {
				String s = sc.nextLine();
				if (s.isEmpty())
					continue;
				Pattern reg = Pattern.compile("\\d+");
				Matcher m = reg.matcher(s);
				m.find();
				int x = Integer.parseInt(m.group()) - 1;
				m.find();
				int y = Integer.parseInt(m.group()) - 1;
				m.find();
				int weight = Integer.parseInt(m.group());
				g.edges[x][y] = weight;
				g.edges[y][x] = weight;
			}
			sc.close();
			return g;
		} catch (FileNotFoundException e) {
			System.out.println("No such graph file.");
			System.exit(1);
		}
		return null;
	}

	/**
	 * Runs the Suurballe algorithm given a source and a destination node.Its
	 * purpose is to find two Link disjoint paths for that pair of nodes in a
	 * non-negative directed graph, such that the sum of their weight is minimum.
	 * 
	 * @param src  The source node
	 * @param dest The destination node
	 * @return The Graph that only contains the two disjoint paths
	 */
	public Graph suurballeGraph(int src, int dest) {
		ArrayList<Integer> path = this.dijkstra(src, dest);
		Graph g = this.copyGraph();
		if (path != null)
			for (int i = 0; i < path.size() - 1; i++) {
				int u = path.get(i);
				int v = path.get(i + 1);
				g.edges[u][v] = 0;
				g.edges[v][u] *= -1;
			}
		path = g.bellmanFord(src, dest);
		Graph g2 = g.copyGraph();
		if (path != null)
			for (int i = 0; i < path.size() - 1; i++) {
				int u = path.get(i);
				int v = path.get(i + 1);
				g2.edges[u][v] = 0;
				g2.edges[v][u] *= -1;
			}
		int n = this.edges.length;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				if (g2.edges[i][j] > 0) {
					g2.edges[i][j] = 0;
					g2.edges[j][i] = 0;
				}
		return g2;
	}

	/**
	 * Runs the Bellman-Ford algorithm. It computes the shortest path from a source
	 * node to a destination node in a graph. Although it is slower than Dijkstra,
	 * it can handle negative weighted edges as opposed to the latter.
	 * 
	 * @param src  The source node
	 * @param dest The destination node
	 * @return The list of integers that denote the node ids that constitute the
	 *         shortest path
	 */
	public ArrayList<Integer> bellmanFord(int src, int dest) {
		int n = edges.length;
		if (n == 0)
			return new ArrayList<Integer>();
		int d[] = new int[n];
		int parents[] = new int[n];

		for (int i = 0; i < n; i++) {
			d[i] = Integer.MAX_VALUE;
			parents[i] = -1;
		}
		d[src] = 0;

		for (int i = 0; i < n; i++)
			for (int u = 0; u < n; u++)
				for (int v = 0; v < n; v++)
					if (edges[u][v] != 0 && d[u] != Integer.MAX_VALUE && d[u] + edges[u][v] < d[v]) {
						d[v] = d[u] + edges[u][v];
						parents[v] = u;
					}
		for (int u = 0; u < n; u++)
			for (int v = 0; v < n; v++)
				if (edges[u][v] != 0 && d[u] != Integer.MAX_VALUE && d[u] + edges[u][v] < d[v]) {
					System.out.println("Negative-weight cycle exists in graph.");
				}

		ArrayList<Integer> path = new ArrayList<Integer>();
		decodeParents(path, parents, src, dest);
		return path.size() > 1 ? path : new ArrayList<Integer>();
	}

	/**
	 * Runs the Dijkstra algorithm. It computes the shortest path from a source node
	 * to a destination node in a graph. It uses the min-heap approach, making it a
	 * very fast.
	 * 
	 * @param src  The source node
	 * @param dest The destination node
	 * @return The list of integers that denote the node ids that constitute the
	 *         shortest path
	 */
	public ArrayList<Integer> dijkstra(int src, int dest) {
		int n = edges.length;
		if (n == 0)
			return new ArrayList<Integer>();
		int d[] = new int[n];
		int parents[] = new int[n];
		Boolean visited[] = new Boolean[n];

		PriorityQueue<Integer> queue = new PriorityQueue<Integer>(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return Integer.compare(d[o1], d[o2]);
			}
		});

		for (int i = 0; i < n; i++) {
			d[i] = Integer.MAX_VALUE;
			visited[i] = false;
			parents[i] = -1;
		}
		d[src] = 0;
		queue.add(src);

		while (queue.isEmpty() != true) {
			int u = queue.poll();
			visited[u] = true;
			for (int v = 0; v < n; v++)
				if (visited[v] != true && edges[u][v] > 0 && d[u] != Integer.MAX_VALUE && d[u] + edges[u][v] < d[v]) {
					d[v] = d[u] + edges[u][v];
					parents[v] = u;
					queue.add(v);
				}
		}

		ArrayList<Integer> path = new ArrayList<Integer>();
		decodeParents(path, parents, src, dest);
		// System.out.println(src+" "+dest+" "+path);
		return path.size() > 1 ? path : new ArrayList<Integer>();
	}

	@SuppressWarnings("unchecked")
	private static boolean samePath(List<Integer> a, Object o2) {
		List<Integer> b;
		try {
			b = (List<Integer>) o2;
		} catch (Exception e) {
			return false;
		}
		if (a.size() != b.size())
			return false;
		for (int i = 0; i < a.size(); i++)
			if (a.get(i) != b.get(i))
				return false;
		return true;
	}

	public List<List<Integer>> yen(int kappa, int src, int dest) {
		List<List<Integer>> yenPaths = new ArrayList<List<Integer>>();
		List<List<Integer>> potentialPaths = new ArrayList<List<Integer>>();
		List<Integer> potentialCosts = new ArrayList<Integer>();

		yenPaths.add(this.dijkstra(src, dest));

		for (int k = 0; k < kappa - 1; k++) {
			Graph temp_graph = this.copyGraph();
			List<Integer> prevPath = yenPaths.get(k);

			for (int i = 0; i < prevPath.size() - 1; i++) {
				int spur = prevPath.get(i);
				List<Integer> rootPath = new ArrayList<Integer>(prevPath.subList(0, i)) {
					private static final long serialVersionUID = 1L;

					@Override
					public boolean equals(Object b) {
						return samePath(this, b);
					}
				};

				// Remove the spur->dest links from previous paths that share the same root path
				for (List<Integer> p : yenPaths) {
					if (i + 1 < p.size()) {
						List<Integer> checkPath = p.subList(0, i);
						if (rootPath.equals(checkPath)) {
							int next = p.get(i + 1);
							temp_graph.edges[spur][next] = 0;
							temp_graph.edges[next][spur] = 0;
						}
					}
				}

				// Remove nodes(except spur) from previous paths that share the same root path
				for (int node : rootPath)
					if (node != spur)
						for (int col = 0; col < temp_graph.edges.length; col++)
							if (col != spur) {
								temp_graph.edges[node][col] = 0;
								temp_graph.edges[col][node] = 0;
							}

				// Find spur path and add it to root
				List<Integer> spurPath = temp_graph.dijkstra(spur, dest);
				if (spurPath.isEmpty() != true) {
					rootPath.addAll(spurPath);
					if (rootPath.isEmpty() != true && potentialPaths.contains(rootPath) != true) {
						potentialPaths.add(rootPath);
						potentialCosts.add(pathDistance(rootPath));
					}
				}

			}
			if (potentialPaths.isEmpty())
				break;
			int index = -1;
			int min = Integer.MAX_VALUE;
			for (int i = 0; i < potentialCosts.size(); i++)
				if (potentialCosts.get(i) < min) {
					min = potentialCosts.get(i);
					index = i;
				}
			yenPaths.add(potentialPaths.get(index));
			potentialPaths.remove(index);
			potentialCosts.remove(index);
		}
		return yenPaths;
	}

	/**
	 * A helping recursive Breadth First Search algorithm. It is used in order to
	 * traverse a graph from a source node until a destination node is found.
	 * 
	 * @param src     The source ndoe.
	 * @param dest    The destination node.
	 * @param visited The nodes that are visited.
	 * @param path    The current path.
	 * @param paths   Found paths so far.
	 */
	private void bfs(int src, int dest, boolean[] visited, ArrayList<Integer> path,
			ArrayList<ArrayList<Integer>> paths) {
		int n = edges.length;
		path.add(src);
		visited[src] = true;
		ArrayList<Integer> adj = new ArrayList<Integer>();
		for (int i = 0; i < n; i++)
			if (this.edges[src][i] != 0)
				adj.add(i);
		for (int d : adj) {
			ArrayList<Integer> path2 = new ArrayList<Integer>();
			for (int node : path)
				path2.add(node);
			if (d != dest && visited[d] == false) {
				visited[d] = true;
				bfs(d, dest, visited, path2, paths);
			} else if (d == dest) {
				path2.add(d);
				paths.add(path2);
			}
		}
		visited[src] = false;
	}

	/**
	 * An algorithm to be used after the use of Suurballe. It will find all possible
	 * paths in a graph that connect the source node with the destination node.
	 * 
	 * @param src  The source node
	 * @param dest The destination node
	 * @return A list of all possible paths in a graph that connect the source node
	 *         with the destination node in a form of a list of integers that denote
	 *         the node id that the path goes through
	 */
	public ArrayList<ArrayList<Integer>> bfsAllPaths(int src, int dest) {
		// printGraph();
		boolean[] visited = new boolean[this.edges.length];
		visited[src] = true;
		ArrayList<ArrayList<Integer>> paths = new ArrayList<ArrayList<Integer>>();
		bfs(src, dest, visited, new ArrayList<Integer>(), paths);
		// System.out.println(paths);
		return paths;
	}

	/**
	 * A helping recursive function in order to decode the parents of the nodes that
	 * the implemented routing algorithms have labeled.
	 * 
	 * @param path    The path to add results in.
	 * @param parents The labeled parents array gotten from a routing algorithm.
	 * @param source  The source node.
	 * @param dest    The destination node.
	 */
	private void decodeParents(ArrayList<Integer> path, int parents[], int source, int dest) {
		int p = parents[dest];
		if (p >= 0)
			decodeParents(path, parents, source, p);
		path.add(dest);
	}

	/**
	 * It calculates the total weight of a path.
	 * 
	 * @param path The path to calculate
	 * @return The weight of the path.
	 */
	public int pathDistance(List<Integer> path) {
		if (path == null)
			return 0;
		int dist = 0;
		for (int i = 0; i < path.size() - 1; i++) {
			int u = path.get(i);
			int v = path.get(i + 1);
			dist += edges[u][v];
		}
		return dist;
	}

	/**
	 * Getter for the edges of the graph.
	 * 
	 * @return The edges of the graph.
	 */
	public int[][] getEdges() {
		return this.edges;
	}

	/**
	 * A function to print the graph in a readable form.
	 */
	public void printGraph() {
		int n = this.edges.length;
		System.out.print("  ");
		for (int j = 0; j < n; j++)
			System.out.print(j + 1 + "\t");
		System.out.println();
		System.out.print("  ");
		for (int j = 0; j < n; j++)
			System.out.print("_\t");
		System.out.println();
		for (int i = 0; i < n; i++) {
			System.out.print(i + 1 + "| ");
			for (int j = 0; j < n; j++)
				System.out.print(this.edges[i][j] + "\t");
			System.out.println();
		}
		System.out.println();
	}
}

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 * The Main class is responsible for all the interaction with the user and for
 * calling the solver when all necessary information have been gathered. It asks
 * for the path of the graph file and the demands file, as well as the code for
 * the type of the algorithm, the type of the backup plan and the first-fit
 * heuristic. It also needs to know the initial and final numbers of demands to
 * read from a file, so that it creates an iteration of experiments that contain
 * those demands. After that, it calls the solver in a loop in order to examine
 * different numbers of demands. It times each solution and holds the results
 * using a Log object, in order to later print them, along with some metrics
 * regarding the performance of each iteration.
 * 
 * @author cmakri07
 *
 */
public class Main {
	/**
	 * The main function that runs the program as described in the class'
	 * description.
	 * 
	 * @param args The initial arguments of the program.
	 */
	public static void main(String[] args) {
		args = readArgs(args);

		String postfix = args[5];
		FileWriter fw_log = null, fw_csv = null;
		try {
			fw_log = new FileWriter(new File("log_" + postfix + ".txt"), true);
			fw_csv = new FileWriter(new File("metrics_" + postfix + ".csv"), true);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Read inputs
		String graphFile = args[0];
		String demandsFile = args[1];
		Demand.FFHeuristic ffHeuristic = Demand.FFHeuristic.getFFHeuristic(args[5]);

		StringBuilder sb_log = new StringBuilder();
		Log.logInputs(graphFile, demandsFile, ffHeuristic, sb_log);

		// Read files
		Graph g = Graph.graphFromFile(graphFile);
		List<Demand> demandsList = Demand.demandsFromFile(demandsFile, 10);

		// Find the numbers to read
		int initDemands = Integer.parseInt(args[2]);
		if (initDemands > demandsList.size())
			initDemands = demandsList.size();
		int endDemands = Integer.parseInt(args[3]);
		if (endDemands > demandsList.size())
			endDemands = demandsList.size();
		else if (endDemands < initDemands)
			endDemands = demandsList.size();
		if (initDemands < 1) {
			initDemands = demandsList.size();
			endDemands = demandsList.size();
		}

		// Preprocess
		for (Demand d : demandsList)
			d.calculatePath(g);

		Demand.sortDemandsList(demandsList, ffHeuristic);

		// Iterate number of demands
		for (; initDemands <= endDemands; initDemands += 1) {
			StringBuilder sb_csv = new StringBuilder();

			// Solve
			if (initDemands % 100 == 0)
				System.out.println("Demands: " + initDemands);

			long time = System.currentTimeMillis();
			List<Link> usedLinks = Link.solveNC(g, 10, demandsList, initDemands);

			time = System.currentTimeMillis() - time;
			// Metrics
			Log log = new Log(demandsList, usedLinks, initDemands, time);

			if (initDemands == endDemands) {
				log.logData(sb_log);
				printSB(fw_log, sb_log);
			}

			log.csvData(sb_csv);
			printSB(fw_csv, sb_csv);
		}

		try {
			fw_csv.close();
			fw_log.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Terminated.");
	}

	/**
	 * A wizard to help the user input the missing arguments to the main program.
	 * 
	 * @param args_old The initial arguments of the program.
	 * @return The altered arguments of the program.
	 */
	private static String[] readArgs(String[] args_old) {
		String[] args = new String[6];
		for (int i = 0; i < args_old.length; i++)
			args[i] = args_old[i];
		Scanner sc = new Scanner(System.in);
		try {
			if (args_old.length < 1) {
				System.out.println("Please enter the graph's file path:");
				args[0] = sc.nextLine();
			}
			if (args_old.length < 2) {
				System.out.println("Please enter the demand's file path:");
				args[1] = sc.nextLine();
			}
			if (args_old.length < 3) {
				System.out.println("Please give M the number of initial demands for the first iteration.");
				System.out.println("( 1 <= M <= max_demands )");
				args[2] = sc.nextInt() + "";
			}
			if (args_old.length < 4) {
				System.out.println("Please give N the number of initial demands for the last iteration.");
				System.out.println("( M <= N <= max_demands )");
				args[3] = sc.nextInt() + "";
			}
			if (args_old.length < 5) {
				System.out.println("Do you want the result to be output into a log file?");
				System.out.println("(Select No for large number of iterations or set higher Heap memory)");
				System.out.println("1. Yes. Save all iterations.");
				System.out.println("2. No. Save only last iteration.");
				args[4] = sc.nextInt() + "";
			}
			if (args_old.length < 6) {
				System.out.println("Please choose the First-Fit heuristic:");
				System.out.println("1. Descending Path size.");
				System.out.println("2. Descending Number of slots.");
				System.out.println("3. Confidential demands last.");
				System.out.println("4. No heuristic (FIFO).");
				System.out.println("5. Hybrid Descending Path size.");
				System.out.println("6. Hybrid Descending Number slots.");
				args[5] = sc.nextInt() + "";
			}
		} catch (Exception e) {
			System.out.println("Wrong input detected.");
			sc.close();
			System.exit(5);
		}
		sc.close();
		return args;
	}

	private static void printSB(FileWriter fw, StringBuilder sb) {
		try {
			fw.append(sb);
			fw.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class represents the links between nodes in each graph. This class is
 * useful for being able to decompose a graph and focus only on the links of the
 * paths that are under examination and therefore match those links with the
 * appropriate connection without violating the given constraints as described
 * in Routing Spectrum Allocation (RSA) Problem. It also holds the solver which
 * runs the two sub problems as described in Project Outline, which result into
 * the final solution of the problem.
 * 
 * @author cmakri07
 *
 */
public class Link implements Comparable<Link> {
	final static int MAX_SLOTS = 320;

	int src;
	int dest;
	int[] slots;

	/**
	 * Constructor of a link.
	 * 
	 * @param src  The source node id
	 * @param dest The destination node id
	 */
	public Link(int src, int dest) {
		this.src = src;
		this.dest = dest;
	}

	/**
	 * A helping function that determines if a link can accept a quantity of slots
	 * that begin at position pos without violating the constraints of continuity or
	 * contiguity,
	 * 
	 * @param pos      The initial position of slot fitting
	 * @param quantity The number of slots to fit.
	 * @return If the link is able to accept the quantity of slots that begin at
	 *         position pos without violating any constraints of continuity.
	 */
	private boolean validIndex(int pos, int quantity) {
		if (this.slots == null)
			return true;
		if (pos + quantity < this.slots.length)
			for (int i = pos; i < pos + quantity; i++)
				if (this.slots[i] != 0)
					return false;
		return true;
	}

	/**
	 * A helping static function to check that all links in a link can accept a
	 * quantity of slots that begin at position pos without violating any
	 * constraints of continuity or contiguity.
	 * 
	 * @param links    The list of links to be checked
	 * @param pos      The initial position of slot fitting
	 * @param quantity The number of slots to fit.
	 * @return If ALL the links are able to accept the quantity of slots that begin
	 *         at position pos without violating any constraints of continuity or
	 *         contiguity.
	 */
	private static boolean checkAllValid(ArrayList<Link> links, int pos, int quantity) {
		for (Link l : links)
			if (l.validIndex(pos, quantity) != true)
				return false;
		return true;
	}

	/**
	 * A helping static function to fill all the links of a demand at a specific
	 * position. It is responsible to call the checkAllValid function until a valid
	 * position is found in order to proceed to the filling, or until the limit of
	 * MAX_SLOTS is reached.
	 * 
	 * @param d     The demand.
	 * @param pos   The value to fill the slots.
	 * @param links The number of slots to fill.
	 * @return If the filling of the links was successful.
	 */
	private static ArrayList<Link> fillLinks(Demand d, int pos, Link[][] links) {
		ArrayList<Link> demandLinks = getLinksFromPath(links, d.path);
		int value = d.id;
		int quantity = d.slotsNumber;

		// Find first available position.
		while (checkAllValid(demandLinks, pos, quantity) != true) {
			if (d.isConfidential())// Conf demands must take exact positions
				return new ArrayList<Link>();
			pos++;
		}

		if (pos + quantity >= MAX_SLOTS)
			return new ArrayList<Link>();

		// Initialize new slots with zeros
		for (Link l : demandLinks) {
			if (l.slots == null) {
				l.slots = new int[MAX_SLOTS];
				for (int i = 0; i < MAX_SLOTS; i++)
					l.slots[i] = 0;
			}
		}

		// Set new slots.
		for (Link l : demandLinks)
			for (int x = pos; x < pos + quantity; x++) {
				l.slots[x] = value;
				d.addSlot(x);
			}

		return demandLinks;
	}

	/**
	 * A static function that given a graph g can create a 2-D array that represents
	 * its links.
	 * 
	 * @param g The graph.
	 * @return The 2-D array of the links of the graph.
	 */
	public static Link[][] createLinks(Graph g) {
		int[][] edges = g.getEdges();
		int n = edges.length;
		Link[][] links = new Link[n][n];
		for (int i = 0; i < n; i++)
			for (int j = i + 1; j < n; j++)
				if (edges[i][j] != 0) {
					links[i][j] = new Link(i, j);
					links[j][i] = new Link(j, i);
				}
		return links;
	}

	/**
	 * A static function that given the links of a graph, and a path, it can return
	 * a list of the links that constitute the path.
	 * 
	 * @param links The links of a graph.
	 * @param path  The path to get the links from.
	 * @return A list of the links that constitute the path.
	 */
	private static ArrayList<Link> getLinksFromPath(Link[][] links, List<Integer> path) {
		ArrayList<Link> list = new ArrayList<Link>();
		for (int i = 0; i < path.size() - 1; i++) {
			int u = path.get(i);
			int v = path.get(i + 1);
			list.add(links[u][v]);
		}
		return list;
	}

	/**
	 * This is the RSA solver function. Given a graph, an empty list of connections,
	 * a filled list of demands and a number of initial demands, it first solves the
	 * network coding routing problem of all its given demands, and finally it
	 * performs the spectrum allocation solution.
	 * 
	 * @param g           The graph.
	 * @param k           The number of shortest paths yen's algorithm finds for
	 *                    confidential demands.
	 * @param initDemands The initial amount of demands for this instance.
	 * @return The list of links that hold the spectrum allocation
	 */
	public static List<Link> solveNC(Graph g, int k, List<Demand> demandsList, int initDemands) {
		ArrayList<Demand> establishedNormalDemands = new ArrayList<Demand>();

		Link[][] links = Link.createLinks(g);
		Set<Link> usedLinks = new HashSet<Link>();
		for (int i = 0; i < initDemands; i++) {
			Demand d = demandsList.get(i);
			d.reset();
			if (d.isConfidential() != true) {
				usedLinks.addAll(fillLinks(d, 0, links));
				establishedNormalDemands.add(d);
			} else {
				ConfDemand cd = (ConfDemand) d;
				List<int[][]> gradeArrays = gradeSlots(cd, establishedNormalDemands, links);
				List<int[][]> groupArrays = groupArrays(gradeArrays, cd);
				List<int[]> minXORs = minXORs(groupArrays);
				int[] maxXorPathGroup = maxXorPathGroup(minXORs);
				cd.setFromYenPath(g, maxXorPathGroup[0], maxXorPathGroup[1]);
				usedLinks.addAll(fillLinks(d, maxXorPathGroup[2], links));
			}
		}

		List<Link> usedLinksList = usedLinks.stream().collect(Collectors.toList());
		usedLinksList.sort(null);
		return usedLinksList;
	}

	/**
	 * It gets the list of minimum XOR operations per group of slots as described in
	 * minXORs() function. It then finds which group gives the largest number of XOR
	 * operations and returns it as an array of int. The array has the maximum
	 * number of XOR operations at its first place, then the index of the path that
	 * gives that number and finally the index of the group that gives that number.
	 * 
	 * @param minXORs The list of minimum XOR operations per group of slots as
	 *                described in minXORs() function.
	 * @return
	 */
	private static int[] maxXorPathGroup(List<int[]> minXORs) {
		int maxPathIndex = -1;
		int maxGroupIndex = -1;
		int max = Integer.MIN_VALUE;
		for (int l = 0; l < minXORs.size(); l++) {
			int[] array = minXORs.get(l);
			for (int i = 0; i < array.length; i++) {
				int val = array[i];
				if (val > max) {
					max = val;
					maxPathIndex = l;
					maxGroupIndex = i;
				}
			}
		}
		int[] ret = { max, maxPathIndex, maxGroupIndex };
//		print(ret);
		return ret;
	}

	/**
	 * A static method that takes the list of grouped arrays as described in
	 * groupArrays() function. It then finds and returns the minimum number of XOR
	 * operations per link for each group of slots as a list.
	 * 
	 * @param groupArrays The list of grouped arrays as described in groupArrays()
	 *                    function.
	 * @return The minimum number of XOR operations per link for each group of slots
	 *         as a list.
	 */
	private static List<int[]> minXORs(List<int[][]> groupArrays) {
		List<int[]> minArrays = new ArrayList<int[]>();

		for (int l = 0; l < groupArrays.size(); l++) {
			int[][] array = groupArrays.get(l);
			int[] minArray = new int[array[0].length];

			for (int j = 0; j < array[0].length; j++) {
				int min = Integer.MAX_VALUE;
				for (int i = 0; i < array.length; i++) {
					int val = array[i][j];
					if (val < min)
						min = val;
				}
				minArray[j] = min;
			}
			minArrays.add(minArray);
		}
//		for (int[] minArray : minArrays)
//			print(minArray);
		return minArrays;
	}

	/**
	 * A static method that takes the list of grade arrays as described in
	 * gradeSlots() function. It then groups the values that indicate the number of
	 * XOR operations per link into groups of the size of the needed slots of the
	 * confidential demand per path. The result shows how many XOR operations per
	 * link can be made on each spectrum slot group per path.
	 * 
	 * @param gradeArrays The list of grade arrays as described in gradeSlots()
	 *                    function.
	 * @param cd          Confidential demand.
	 * @return A list of arrays which show how many XOR operations per link can be
	 *         made on each spectrum slot group for each grade array (path).
	 */
	private static List<int[][]> groupArrays(List<int[][]> gradeArrays, ConfDemand cd) {
		List<int[][]> groupedArrays = new ArrayList<int[][]>();

		for (int l = 0; l < gradeArrays.size(); l++) {
			int slotsNumber = cd.slotNumbersPerPath.get(l);
			int[][] array = gradeArrays.get(l);
			int[][] groupArray = new int[array.length][array[0].length - slotsNumber + 1];
			for (int i = 0; i < array.length; i++) {
				for (int j = 0; j < array[0].length - slotsNumber; j++) {
					int counter = 0;
					for (int k = 0; k < slotsNumber; k++) {
						int val = array[i][j + k];
						if (counter >= 0 && val >= 0)
							counter += val;
						else
							counter = -1;
					}
					groupArray[i][j] = counter;
				}
			}
			groupedArrays.add(groupArray);
		}
		return groupedArrays;
	}

	/**
	 * A static function that grades the needed slots of a confidential demand. For
	 * every possible path of the confidential demand, it creates a list of
	 * sub-demands which diverge from the under examination confidential path and
	 * then merge again with it. For each such sub-demand, a grade array is being
	 * created which holds the number of alternate paths of established normal
	 * demand and thus can be used as the split signals of the confidential demand.
	 * 
	 * @param confidentialDemand       The confidential demand.
	 * @param establishedNormalDemands The list of normal demands.
	 * @param links                    The links of the problem.
	 * @return A list of grade arrays that hold how many alternate paths of normal
	 *         demands can run through each slot of the sub-demand.
	 */
	private static List<int[][]> gradeSlots(ConfDemand confidentialDemand, List<Demand> establishedNormalDemands,
			Link[][] links) {
		List<int[][]> gradeArrays = new ArrayList<int[][]>();
		for (List<Integer> path : confidentialDemand.yenPaths) {
			int[][] array = new int[path.size() - 1][MAX_SLOTS];
			gradeArrays.add(array);

			for (int[] row : array)
				for (int i = 0; i < row.length; i++)
					row[i] = 0;
			List<SubDemand> subDemands = ConfDemand.findCommonSubDemands(establishedNormalDemands, path);
			for (SubDemand sd : subDemands) {
				for (Link l : getLinksFromPath(links, sd.confPath))
					if (sd.path.size() > 2) {
						for (int slot : sd.slots)
							if (array[path.indexOf(l.src)][slot] != -1)
								array[path.indexOf(l.src)][slot] += 1;
					} else {
						for (int slot : sd.slots)
							array[path.indexOf(l.src)][slot] = -1;
					}
			}
//			print(array);
		}
		return gradeArrays;
	}

	/**
	 * Helping function to print an int array of one dimension.
	 * 
	 * @param array Array of one dimension.
	 */
	private static void print(int[] array) {
		for (int x : array)
			System.out.print(x + " ");
		System.out.println();
	}

	/**
	 * Helping function to print an int array of two dimensions.
	 * 
	 * @param array Array of two dimensions.
	 */
	@SuppressWarnings("unused")
	private static void print(int[][] array) {
		for (int[] row : array) {
			print(row);
		}
		System.out.println();
	}

	@Override
	public int compareTo(Link o) {
		int a = Integer.compare(this.src, o.src);
		if (a == 0)
			return Integer.compare(this.dest, o.dest);
		return a;
	}

	@Override
	public String toString() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int x : this.slots)
			list.add(x);
		return (this.src + 1) + "," + (this.dest + 1) + ":\t" + list;
	}
}

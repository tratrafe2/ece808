import java.util.ArrayList;
import java.util.List;

/**
 * The Confidential Demand class which calculates the K-shortest paths using
 * Yen’s algorithm. What is more, after the first fit algorithm has chosen which
 * path is the best in order to optimize slot allocation, it holds how many XOR
 * operations can be made using the chosen path
 * 
 * @author cmakri07
 *
 */
public class ConfDemand extends Demand {

	List<List<Integer>> yenPaths;
	List<Integer> slotNumbersPerPath;
	int XORs;
	int k;

	public ConfDemand(int src, int dest, int bitRate, int id, int k) {
		super(src, dest, bitRate, id);
		this.slotNumbersPerPath = new ArrayList<Integer>();
		this.k = k;
		this.reset();
	}

	/**
	 * Sets the number of XOR operations that a specific path from the list of yen
	 * paths can hold.
	 * 
	 * @param g        The graph.
	 * @param XORs     The number of XOR operations of the specific path
	 * @param yenIndex The index of the specific path
	 */
	public void setFromYenPath(Graph g, int XORs, int yenIndex) {
		this.XORs = XORs;
		this.path = yenPaths.get(yenIndex);
		this.slotsNumber = slotNumbersPerPath.get(yenIndex);
	}

	/**
	 * A helping static function that finds sub-demands of a path based on the list
	 * of normal demands of the problem. More specifically it checks the path of
	 * every normal demand to see if it can hold a path that diverges from the
	 * original confidential path and then merges again with it.
	 * 
	 * @param normalDemands The list of normal demands of the problem.
	 * @param cPath         the original confidential path.
	 * @return The list of sub-demands.
	 */
	public static List<SubDemand> findCommonSubDemands(List<Demand> normalDemands, List<Integer> cPath) {
		List<SubDemand> subDemands = new ArrayList<SubDemand>();
//		System.out.println(cPath);
		for (Demand d : normalDemands) {
//			System.out.println("ND: " + d);
			int currentNode = -1;
			int curIndexND = 0;
			int curIndexCONF = 0;
			for (int j = 0; j < d.path.size(); j++) {
				int nodeND = d.path.get(j);
				for (int i = curIndexCONF; i < cPath.size(); i++) {
					int nodeCONF = cPath.get(i);

					if (nodeND == nodeCONF) {
						if (currentNode > -1) {
							int nextIndexND = d.path.indexOf(nodeND);
							int nextIndexCONF = cPath.indexOf(nodeND);
							List<Integer> sublistND = d.path.subList(curIndexND, nextIndexND + 1);
							List<Integer> sublistCONF = cPath.subList(curIndexCONF, nextIndexCONF + 1);
//							System.out.println("\t\tSD: " + sublistND + "\t" + sublistCONF);
							subDemands.add(new SubDemand(d, sublistND, sublistCONF));
						}
						curIndexND = d.path.indexOf(nodeND);
						curIndexCONF = cPath.indexOf(nodeND);
						currentNode = nodeND;
						break;
					}
				}
			}
		}
		return subDemands;
	}

	@Override
	public void reset() {
		this.slots.clear();
		this.path = new ArrayList<Integer>();
		this.XORs = 0;
		this.slotsNumber = -1;
	}

	@Override
	public boolean isConfidential() {
		return true;
	}

	@Override
	public void calculatePath(Graph g) {
		findPath(g, src, dest);
		this.slotsNumber = -1;
	}

	@Override
	protected void findPath(Graph g, int src, int dest) {
		this.yenPaths = g.yen(k, src, dest);
		for (List<Integer> path : this.yenPaths) {
			int distance = g.pathDistance(path);
			this.slotNumbersPerPath.add(calculateSlotsNumber(bitRate, distance));
		}
	}

	@Override
	public String toString() {
		return super.toString().replace("][_]", "][" + this.XORs + "]");
	}

}

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * This class exists for the creation of a log object that keeps track of the
 * metrics of an instance of a solution. This is essential in order to be able
 * to keep the results of each iteration of the loop that calls the solve
 * function with different numbers of demands each time.
 * 
 * @author cmakri07
 *
 */
public class Log {

	List<Link> usedLinks;
	List<Demand> demandsList;
	int initConnections;
	long time;

	int normDemands = 0;
	int normRouted = 0;
	int normSlots = 0;

	int confDemands = 0;
	int confRouted = 0;
	int confSlots = 0;

	double avgXor = 0.0;

	/**
	 * The constructor of the Log object.
	 * 
	 * @param connectionList  The list of connections.
	 * @param usedLinks       The list of Used links.
	 * @param initConnections The number of initial demands.
	 * @param time            The computation time.
	 */
	Log(List<Demand> demandsList, List<Link> usedLinks, int initConnections, long time) {
		this.demandsList = demandsList;
		this.usedLinks = usedLinks;
		this.initConnections = initConnections;
		this.time = time;
		calcMetrics(initConnections);

	}

	/**
	 * A private helping function to calculate this log's metrics.
	 * 
	 * @param initDemands the number of demands to read.
	 */
	private void calcMetrics(int initDemands) {
		for (int i = 0; i < initDemands; i++) {
			Demand d = demandsList.get(i);
			if (d.isConfidential()) {
				this.confDemands++;
				if (d.getRouted()) {
					this.confSlots += d.slotsNumber;
					this.confRouted++;
					avgXor += ((ConfDemand) d).XORs;
				}
			} else {
				this.normDemands++;
				if (d.getRouted()) {
					this.normSlots += d.slotsNumber;
					this.normRouted++;
				}
			}
		}
		avgXor = 1.0 * avgXor / confRouted;
	}

	/**
	 * A static function to log the inputs of an instance.
	 * 
	 * @param graphFile   The graph file path.
	 * @param demandsFile The demands file path.
	 * @param ffHeuristic The heuristic of the first fit algorithm.
	 * @param sb_log      The StringBuilder to append this information.
	 */
	public static void logInputs(String graphFile, String demandsFile, Demand.FFHeuristic ffHeuristic,
			StringBuilder sb_log) {
		sb_log.append("Graph file: " + graphFile + "\n");
		sb_log.append("Demands file: " + demandsFile + "\n");
		sb_log.append("First-Fit heuristic: " + ffHeuristic + "\n");
	}

	/**
	 * A function to log the properties of a log object into a stringBuilder.
	 * 
	 * @param sb_log The StringBuilder to append the log's information.
	 */
	public void logData(StringBuilder sb_log) {
		sb_log.append("\nID\t[Rtd][XOR]\tSlots\tBitrate\tPath\n");
		List<Demand> demands = new ArrayList<Demand>(this.demandsList);
		demands.sort(new Comparator<Demand>() {
			@Override
			public int compare(Demand o1, Demand o2) {
				return Integer.compare(o1.id, o2.id);
			}
		});

		for (Demand d : demands)
			if (d.path != null && d.path.size() != 0)
				sb_log.append(d + "\n");

		sb_log.append("\nLinks\n");
		for (Link l : usedLinks)
			if (l.slots != null)
				sb_log.append(l + "\n");

		double connectionBlockingRate = 100.0 * (this.initConnections - this.confRouted - this.normRouted)
				/ this.initConnections;

		sb_log.append("\n");
		sb_log.append("Number of initial demands: " + this.initConnections + "\n");
		sb_log.append("Number of routed demands: " + (this.normRouted + this.confRouted) + "\n");
		sb_log.append("\nComputation time: " + this.time + " ms" + "\n");

		sb_log.append("\nNumber of confidential demands: " + this.confDemands + "\n");
		sb_log.append("Number of routed confidential demands: " + this.confRouted + "\n");
		sb_log.append("Number of utilized slots in confidential demands: " + this.confSlots + "\n");

		sb_log.append("\nNumber of normal demands: " + this.normDemands + "\n");
		sb_log.append("Number of routed normal demands: " + this.normRouted + "\n");
		sb_log.append("Number of utilized slots in normal demands: " + this.normSlots + "\n");

		sb_log.append("\nAverage XOR per confidential demand: " + this.avgXor + "\n");

		sb_log.append("\nNot routed demands: " + (this.initConnections - this.confRouted - this.normRouted) + "\n");
		sb_log.append("Connection Blocking rate: " + connectionBlockingRate + "\n");
	}

	/**
	 * A function to log the properties of a log object into a stringBuilder for a
	 * csv form.
	 * 
	 * @param sb_csv The StringBuilder to append the log's information in csv form.
	 */
	public void csvData(StringBuilder sb_csv) {
		sb_csv.append(this.initConnections + ",");
		sb_csv.append(this.time + ",");

		sb_csv.append(this.confDemands + ",");
		sb_csv.append(this.confRouted + ",");
		sb_csv.append(this.confSlots + ",");

		sb_csv.append(this.normDemands + ",");
		sb_csv.append(this.normRouted + ",");
		sb_csv.append(this.normSlots + ",");

		sb_csv.append(this.avgXor + "\n");

	}
}

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * This class is responsible for representing the connection demands and
 * therefore hold information about the source, destination and bitrate of the
 * requested connections. What is more, it uses the algorithms from the
 * Graph.java file to calculate the primary and backup paths of the connections
 * depending on the algorithm and backup plan that the user has stated, as well
 * as the number of slots that they need. It is the link between the user
 * options given at the interface and the processes to be performed by the
 * program.
 * 
 * @author cmakri07
 *
 */
public class Demand {
	private static double baudRate = 10.7; // Gbaud for 12.5 Ghz

	int id;
	int src;
	int dest;

	int bitRate;

	int slotsNumber;
	List<Integer> path;

	protected Set<Integer> slots;

	/**
	 * Demand constructor
	 * 
	 * @param src     Source node of demand
	 * @param dest    Destination node of demand
	 * @param bitRate BitRate of demand
	 * @param id      ID of demand
	 */
	public Demand(int src, int dest, int bitRate, int id) {
		this.id = id;
		this.src = src;
		this.dest = dest;
		this.bitRate = bitRate;
		this.path = new ArrayList<Integer>();
		this.slots = new HashSet<Integer>();
	}

	/**
	 * It calculates the main and backup path of the demand if they exist by calling
	 * the findPaths function given a graph, an algorithm and a backup method.
	 * Finally, it uses the calculateSlotsNumber function to store the number of
	 * slots its main path needs.
	 * 
	 * @param g The graph
	 */
	public void calculatePath(Graph g) {
		findPath(g, src, dest);
		int distance = g.pathDistance(this.path);
		this.slotsNumber = calculateSlotsNumber(bitRate, distance);
	}

	/**
	 * A static function to calculate the bits per symbol given the distance of a
	 * path.
	 * 
	 * @param distance The distance.
	 * @return Bits per symbol.
	 */
	private static int findBitsPerSymbol(int distance) {
		if (distance <= 800) // 16QAM
			return 4;
		if (distance <= 1700) // 8QAM
			return 3;
		if (distance <= 4600) // QPSK
			return 2;
		if (distance <= 9300) // BPSK
			return 1;
		return 0;
	}

	/**
	 * A function that given an algorithm, a backup method, a graph and a source and
	 * destination pair, it calculates the available path/paths of that pair. It is
	 * responsible to set both the main and the backup paths of the demand if
	 * possible.
	 * 
	 * @param g    The graph.
	 * @param src  The source node.
	 * @param dest The destination node.
	 */
	protected void findPath(Graph g, int src, int dest) {
		this.path = g.dijkstra(src, dest);
	}

	/**
	 * A function that reads the demands as given in a file. It only reads Integer
	 * values and discards any other characters.
	 * 
	 * @param filePath The file path.
	 * @param k        The number of yen's algorithm shortest paths.
	 * @return The list of demands.
	 */
	public static List<Demand> demandsFromFile(String filePath, int k) {
		List<Demand> list = new ArrayList<Demand>();
		try {
			Scanner sc = new Scanner(new File(filePath));
			sc.nextInt(); // To read the number of demands
			int i = 1;
			while (sc.hasNextLine()) {
				int x = sc.nextInt() - 1;
				int y = sc.nextInt() - 1;
				int bitRate = sc.nextInt();
				Demand d = sc.nextInt() == 1 ? new ConfDemand(x, y, bitRate, i++, k) : new Demand(x, y, bitRate, i++);
				list.add(d);
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println("No such demand file.");
			System.exit(2);
		}
		return list;
	}

	/**
	 * A getter to determine if the demand is routed.
	 * 
	 * @return if the demand is routed.
	 */
	public boolean getRouted() {
		return this.slots.isEmpty() != true;
	}

	/**
	 * A static function to calculate the number of slots that a demand needs based
	 * on the bitrate, the distance and the global baudRate.
	 * 
	 * @param bitRate  The bitrate.
	 * @param distance The distance.
	 * @return The number of slots.
	 */
	protected static int calculateSlotsNumber(int bitRate, int distance) {
		return (int) Math.ceil((1.0 * bitRate) / (baudRate * findBitsPerSymbol(distance)));
	}

	/**
	 * The heuristic function that depending on the FFHeuristic, it calls the
	 * appropriate function in order to sort the list of demands as needed.
	 * 
	 * @param demandsList The list of demands
	 * @param ffHeuristic The heuristic
	 */
	public static void sortDemandsList(List<Demand> demandsList, FFHeuristic ffHeuristic) {
		switch (ffHeuristic) {
		case DESC_PATH_SIZE:
			descPathSize(demandsList);
			break;
		case DESC_NUM_SLOTS:
			descSlotNumber(demandsList);
			break;
		case CONF_LAST:
			sortConf(demandsList, false);
			break;
		case HYBRID_PATH_SIZE:
			descPathSize(demandsList);
			shuffleConfNorm(demandsList);
			break;
		case HYBRID_NUM_SLOTS:
			descSlotNumber(demandsList);
			shuffleConfNorm(demandsList);
			break;
		case FIFO:
		default:
		}
	}

	/**
	 * Brings confidential demands first or last based on the boolean on the demands
	 * list.
	 * 
	 * @param list  The demands list.
	 * @param first True if the confidential demands need to be first, false if are
	 *              needed to be last.
	 */
	private static void sortConf(List<Demand> list, boolean first) {
		List<Demand> temp = new ArrayList<Demand>();
		for (Demand d : list) {
			if (d.isConfidential() == first)
				temp.add(0, d);
			else
				temp.add(d);
		}
		list.clear();
		list.addAll(temp);
	}

	/**
	 * Sorts demands randomly based on their hashcode value.
	 * 
	 * @param list The list of demands.
	 */
	@SuppressWarnings("unused")
	private static void randomSort(List<Demand> list) {
		Collections.sort(list, new Comparator<Demand>() {
			@Override
			public int compare(Demand o1, Demand o2) {
				return Integer.compare(o1.hashCode(), o2.hashCode());
			}
		});
	}

	/**
	 * Shuffles confidential and normal demands while keeping their group sorting.
	 * It assumes that the demands list is already sorted by groups.
	 * 
	 * @param list The list of demands.
	 */
	private static void shuffleConfNorm(List<Demand> list) {
		List<Demand> conf = new ArrayList<Demand>();
		List<Demand> norm = new ArrayList<Demand>();
		for (Demand d : list)
			if (d.isConfidential())
				conf.add(d);
			else
				norm.add(d);
		list.clear();
		while (conf.isEmpty() != true || norm.isEmpty() != true) {
			if (norm.isEmpty() == true)
				list.add(conf.remove(0));
			else if (conf.isEmpty() == true)
				list.add(norm.remove(0));
			else if (Math.random() > 0.5)
				list.add(conf.remove(0));
			else
				list.add(norm.remove(0));
		}
	}

	/**
	 * A helping heuristic function that sorts a list of demands descending
	 * depending on their needed slot number.
	 * 
	 * @param list The list of demands
	 */
	private static void descSlotNumber(List<Demand> list) {
		Collections.sort(list, new Comparator<Demand>() {
			@Override
			public int compare(Demand o1, Demand o2) {
				int i = -Integer.compare(o1.slotsNumber, o2.slotsNumber);
				if (i == 0)
					return -Integer.compare(o1.path.size(), o2.path.size());
				return i;
			}
		});
	}

	/**
	 * A helping heuristic function that sorts a list of demands descending
	 * depending on their path size.
	 * 
	 * @param list The list of demands
	 */
	private static void descPathSize(List<Demand> list) {
		Collections.sort(list, new Comparator<Demand>() {
			@Override
			public int compare(Demand o1, Demand o2) {
				int i = -Integer.compare(o1.path.size(), o2.path.size());
				if (i == 0)
					return -Integer.compare(o1.slotsNumber, o2.slotsNumber);
				return i;
			}
		});
	}

	@Override
	public String toString() {
		if (this.path == null || this.path.size() == 0)
			return "Undefined path";
		ArrayList<Integer> tempPath = new ArrayList<Integer>();
		for (int x : this.path)
			tempPath.add(x + 1);
		return this.id + "\t" + (this.getRouted() == true ? "[R][_]" : "[_][_]") + "\t\t" + this.slotsNumber + "\t\t"
				+ this.bitRate + "\t\t" + tempPath.get(0) + " -> " + tempPath.get(tempPath.size() - 1) + "\t" + tempPath
				+ "\t\tSlots:\t " + this.slots;
	}

	/**
	 * Enum class to distinguish the different heuristics for the First Fit
	 * algorithm.
	 * 
	 * @author cmakri07
	 *
	 */
	public enum FFHeuristic {
		DESC_PATH_SIZE, DESC_NUM_SLOTS, FIFO, CONF_LAST, HYBRID_PATH_SIZE, HYBRID_NUM_SLOTS;

		public static FFHeuristic getFFHeuristic(String choice) {
			int c = Integer.parseInt(choice);
			switch (c) {
			case 1:
				return DESC_PATH_SIZE;
			case 2:
				return DESC_NUM_SLOTS;
			case 3:
				return CONF_LAST;
			case 4:
				return FIFO;
			case 5:
				return HYBRID_PATH_SIZE;
			case 6:
				return HYBRID_NUM_SLOTS;
			default:
				System.out.println("Wrong type of First Fit Heuristic.");
				System.exit(4);
			}
			return null;
		}
	}

	/**
	 * Adds a specific value to the slots of the demand.
	 * 
	 * @param x The slot id to add.
	 */
	public void addSlot(int x) {
		this.slots.add(x);
	}

	/**
	 * Checks if the demand is confidential.
	 * 
	 * @return If the demand is confidential.
	 */
	public boolean isConfidential() {
		return false;
	}

	/**
	 * Resets the demand.
	 */
	public void reset() {
		this.slots.clear();
	}
}

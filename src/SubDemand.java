import java.util.List;

/**
 * A helping class responsible to allow the first fit algorithm to choose which
 * path is the best for each confidential demand.
 * 
 * @author cmakri07
 *
 */
public class SubDemand extends Demand {

	List<Integer> confPath;

	public SubDemand(Demand d, List<Integer> path, List<Integer> confPath) {
		super(d.src, d.dest, d.bitRate, d.id);
		this.slotsNumber = d.slotsNumber;
		this.slots = d.slots;
		this.path = path;
		this.confPath = confPath;
	}

}

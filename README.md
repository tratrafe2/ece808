# ECE808 Project implementation

## Introduction
The Routing Spectrum Allocation (RSA) problem is a computational problem that given a set of connections in a network, it tries to allocate the frequency slots necessary in order to establish these connections. At the same time it guarantees that there will be no overlaps or discontinuities between those frequencies. Such a problem can be solved either dynamically or statically.
A dynamic approach to the problem can imply complex networks and demands that vary over time. This means that a frequency allocated for a connection at a given time, might not be suitable if chosen to be allocated at a later time.
On the other hand, for a static approach to the problem, a static network and a static set of demands need to be taken into consideration prior to the computation of the Routing Spectrum Allocation. As a result, during a precomputation process, there can be a solution that indeed does not violate any of the constraints listed above. There are various methods for finding a solution, such as a formulation of the problem to Integer Linear Programming (ILP), or by decomposing the routing and spectrum allocation sub problems in order to solve them independently, one after the other.
Moreover, by taking it a step further, such networks might be vulnerable to eavesdropping. As a result, various techniques are being developed in order to minimize the risk. One such technique is the Network Coding (NC), which provides a physical layer security to the network by splitting confidential connections into XOR-ed signals across different nodes in their path. That way, only someone that has collected all the individual signals can decrypt the original signal, something that would be really hard for an eavesdropper. 
For the purposes of this project, the NC-RSA problem will be examined, focusing only to the static approach for the RSA and having various heuristics for prioritizing confidential demands.

## Motivation
The Routing Spectrum Allocation (RSA) problem for static optical connections is a common question in the field of optical networking.  Bandwidth increases rapidly as time goes by, due to modern demands of multimedia through the internet. As a result, networks that serve these needs, require to follow such growth in order to succeed to their purpose, while being flexible, efficient and cost effective. 
Moreover, security is a crucial aspect of communication and thus such networks must take it into consideration. As a result, various techniques are being developed in order to make communication more secure. Although these techniques offer some enhanced security, in most cases they come with some tradeoffs. Therefore, this is the case for the Network Coding technique for the RSA problem.
Finally, network problems combine a vast spectrum of disciplines and are undoubtedly crucial for the development of the modern world. What is more, ensuring their confidentiality is as essential as their existence itself. As a result, by examining in depth the wonders of such disputes is essential for both the scientific community and the society as a whole.

## Objectives
The objective of this project is to implement the problem of Network Coding - Routing and Spectrum Allocation (NC-RSA) for static optical connections and finally result in conclusions for different network topologies through experimental procedures. The results will give an overview over which heuristics for prioritizing confidential and non-confidential demands will have the best performance or the lowest tradeoffs in terms of how these connections can be assigned wavelengths in such a way that efficiency and reliability are guaranteed.

## Installation and Usage
In order to compile the source files and run the program on command line, change directory to the src folder and execute the javac and java commands:

```bash
cd src
javac *.java
java Main 
```

From here on, follow the on-screen instructions to enter the arguments needed.
The arguments can be also entered through the command line. The user can skip from one to all the arguments, beginning from the rightmost.

```bash
java Main [G] [D] [M] [N] [L] [H]
```
[G]: The Graph's file location.

[D]: The demand's file location.

[M]: The number of initial demands for the first iteration. <br />
&nbsp;&nbsp;&nbsp;&nbsp; 1 <= M <= max_demands <br />

[N]: The number of initial demands for the last iteration. <br />
&nbsp;&nbsp;&nbsp;&nbsp; M <= N <= max_demands  <br />

[L]: Option to Log the answer to a file:
(Select No for large number of iterations or set higher Heap memory)
1. Yes. Save all iterations.
2. No. Save only last iteration.

[H]: The enumerations of first-fit Heuristics:
1.	Descending Path size.
2.	Descending Number of slots.
3.	Confidential demands last.
4.	No heuristic (FIFO).
5.	Hybrid Descending Path size.
6.	Hybrid Descending Number slots.


The program will iterate N-M+1 times. Each iteration will read M+i demands, where i is the number of the iteration. <br />
For example:
 
```bash
java Main graph.txt demands.txt 5 100 2 1 
```

This command will run the solver 96 times for the files: graph.txt and demands.txt. The first time will be reading the first 5 demands of the file, while each subsequent iteration will have one more demand, leading to reading the first 100 demands of the file at the 96th iteration. The extended results will not be logged into a file and the heuristic will be the one depending on the path cost.